﻿using ReGoap.Core;
using System.Collections.Generic;

namespace ReGoap.Unity.Test
{
    /// <summary>
    /// Action测试类
    /// </summary>
    public class ReGoapTestAction : ReGoapAction<string, object>
    {
        public delegate void PreconditionsGetter(ref ReGoapState<string, object> preconditions,
            GoapActionStackData<string, object> stackData);

        public delegate void EffectsGetter(ref ReGoapState<string, object> effects,
            GoapActionStackData<string, object> stackData);

        public delegate void CostGetter(ref float cost, GoapActionStackData<string, object> stackData);

        public delegate List<ReGoapState<string, object>> SettingsGetter(GoapActionStackData<string, object> stackData);

        /// <summary>
        /// 获取先决条件时会执行的委托（用于传递自定义获取方案）
        /// </summary>
        public PreconditionsGetter CustomPreconditionsGetter;

        /// <summary>
        /// 获取效果时会执行的委托（用于传递自定义获取方案）
        /// </summary>
        public EffectsGetter CustomEffectsGetter;

        /// <summary>
        /// 获取权重时会执行的委托（用于传递自定义获取方案）
        /// </summary>
        public CostGetter CustomCostGetter;

        /// <summary>
        /// 获取设置时会执行的委托（用于传递自定义获取方案）
        /// </summary>
        public SettingsGetter CustomSettingsGetter;

        public void Init()
        {
            Awake();
        }

        /// <summary>
        /// 设置效果
        /// </summary>
        /// <param name="effects"></param>
        public virtual void SetEffects(ReGoapState<string, object> effects)
        {
            this.effects = effects;
        }

        /// <summary>
        /// 设置先决条件
        /// </summary>
        /// <param name="preconditions"></param>
        public virtual void SetPreconditions(ReGoapState<string, object> preconditions)
        {
            this.preconditions = preconditions;
        }

        /// <summary>
        /// 获取设置
        /// </summary>
        /// <param name="stackData"></param>
        /// <returns></returns>
        public override List<ReGoapState<string, object>> GetSettings(GoapActionStackData<string, object> stackData)
        {
            //会先尝试用自定义SettingGetter来获取设置
            if (CustomSettingsGetter != null)
                return CustomSettingsGetter(stackData);
            return new List<ReGoapState<string, object>> {settings};
        }

        /// <summary>
        /// 获取先决条件
        /// </summary>
        /// <param name="stackData"></param>
        /// <returns></returns>
        public override ReGoapState<string, object> GetPreconditions(GoapActionStackData<string, object> stackData)
        {
            if (CustomPreconditionsGetter != null)
                CustomPreconditionsGetter(ref preconditions, stackData);
            return preconditions;
        }

        /// <summary>
        /// 获取效果
        /// </summary>
        /// <param name="stackData"></param>
        /// <returns></returns>
        public override ReGoapState<string, object> GetEffects(GoapActionStackData<string, object> stackData)
        {
            if (CustomEffectsGetter != null)
                CustomEffectsGetter(ref effects, stackData);
            return effects;
        }

        /// <summary>
        /// 获取权重（越小越受青睐）
        /// </summary>
        /// <param name="stackData"></param>
        /// <returns></returns>
        public override float GetCost(GoapActionStackData<string, object> stackData)
        {
            if (CustomCostGetter != null)
                CustomCostGetter(ref Cost, stackData);
            return Cost;
        }
    }
}